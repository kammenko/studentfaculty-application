package com.kirey.test;

import com.kirey.test.model.entities.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {


        SpringApplication.run(TestApplication.class, args);
    }

}
