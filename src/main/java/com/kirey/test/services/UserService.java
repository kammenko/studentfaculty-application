package com.kirey.test.services;

import com.kirey.test.model.dao.StudentDAO;
import com.kirey.test.model.dao.StudentDAOImpl;
import com.kirey.test.model.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    private static StudentDAOImpl studentDAO;

    public UserService() {
        studentDAO = new StudentDAOImpl();
    }

    public Student getById(Long id){
        studentDAO.openCurrentSession();
        Student student = studentDAO.findStudentByid(id);
        studentDAO.closeCurrentSession();
        return student;

    }

    public Student saveStudent(Student student){
        studentDAO.openCurrentSessionWithTransaction();
        student = studentDAO.save(student);
        studentDAO.closCurrentSessionWithTransaction();
        return student;
    }
}
