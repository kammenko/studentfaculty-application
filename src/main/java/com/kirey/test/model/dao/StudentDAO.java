package com.kirey.test.model.dao;

import com.kirey.test.model.entities.Student;

import java.io.Serializable;
import java.util.List;

public interface StudentDAO<T, Id extends Serializable> {


    Student findStudentByid(Long id);
    List<Student> findAllStudents();
    Student save(Student student);
    Student update(Student student);
    void delete(Student student);
    Student getStudentbyJMBG(String jmbg);

}
