package com.kirey.test.model.dao;

import com.kirey.test.model.entities.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.core.serializer.Deserializer;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class StudentDAOImpl implements StudentDAO<Student, Long> {

    private Session currentSession;

    private Transaction currentTransaction;


    public StudentDAOImpl() {
    }


    private static SessionFactory getSessionFactory(){
        Configuration configuration = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        return sessionFactory;

    }

    public Session openCurrentSession(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return  currentSession;

    }

    public Session openCurrentSessionWithTransaction(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public Student findStudentByid(Long id) {
        Student student = (Student) getCurrentSession().get(Student.class, id);
        return student;
    }

    @Override
    public List<Student> findAllStudents() {
        List<Student> students = (List<Student>) getCurrentSession().createQuery("from students").list();

        return students;
    }

    @Override
    public Student save(Student student) {

       student.setId((Long)getCurrentSession().save(student));
        return student;
    }

    @Override
    public Student update(Student student) {
        return null;
    }

    @Override
    public void delete(Student student) {

    }

    @Override
    public Student getStudentbyJMBG(String jmbg) {
        return null;
    }
}
