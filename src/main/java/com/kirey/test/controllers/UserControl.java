package com.kirey.test.controllers;

import com.kirey.test.model.entities.Student;
import com.kirey.test.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/")
public class UserControl {

    @Autowired
    UserService service;

    @GetMapping(value = "get/{id}")
    public Student getStudent(@PathVariable("id")Long id){

        return service.getById(id);
    }

    @PostMapping(value = "/save")
    public Student saveStudent(Student student){
        System.out.println(student);
        student=service.saveStudent(student);
        return student;
    }
}
