

CREATE TABLE roles(
    id bigint(20) auto_increment primary key,
    name varchar(255)
);

CREATE TABLE users(
                      id bigint(20) auto_increment primary key,
                      username varchar(255),
                      password varchar(255),
                      roleId bigint(20),
                      constraint fk1 foreign key (roleId) REFERENCES roles(id)
);

CREATE TABLE students(
    id bigint(20) auto_increment primary key,
    name varchar(255),
    lastName varchar(255),
    jmbg varchar(13),
    birthday varchar(20),
    address varchar(255),
    email varchar(255),
    phone varchar(255)
);

CREATE TABLE faculties(
    id bigint(20) auto_increment primary key,
    name varchar(255),
    address varchar(255),
    email varchar(255),
    phone varchar(255)
);

CREATE TABLE students_faculties(
    student bigint(20) ,
    faculty bigint(20),
    PRIMARY KEY(student, faculty),
    constraint fk2 foreign key (student) REFERENCES students(id),
    constraint fk3 foreign key (faculty) REFERENCES faculties(id)
);